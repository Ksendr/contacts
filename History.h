//
//  History.h
//  contacts
//
//  Created by Ksendr on 02.11.14.
//  Copyright (c) 2014 Ksendr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Contact.h"

@interface History : NSObject

@property NSMutableDictionary *dict;

-(id)init:(NSUInteger)capacity;
-(void) addInHistory:(Contact *)contact;
-(NSArray *)arrayCurrentKey:(NSInteger)key;
-(void)clear;

@end
