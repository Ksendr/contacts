//
//  ContactsTableViewController.h
//  contacts
//
//  Created by Ksendr on 01.11.14.
//  Copyright (c) 2014 Ksendr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Contact.h"
#import "History.h"

@interface ContactsTableViewController : UITableViewController

@property History *history;
-(void) addContact:(NSString *)name :(NSString *)phone;

@end
