//
//  Contact.m
//  contacts
//
//  Created by Ksendr on 01.11.14.
//  Copyright (c) 2014 Ksendr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Contact.h"

@implementation Contact

-(id)init:(NSString *)name :(NSString *)phone{
    self = [super init];
    if(self){
        _name = name;
        _phone = phone;
    }
    return self;
}

-(void)call{
    // initiate a real call (not working in simulator)
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", self.phone]]];
    
    UIAlertView *fakeCall = [[UIAlertView alloc] initWithTitle:@"Fake call"
                                                       message:[NSString stringWithFormat:@"Phoning %@", self.name]
                                                      delegate:nil
                                             cancelButtonTitle:@"Back"
                                             otherButtonTitles:nil];
    [fakeCall show];
}

@end
