//
//  AddViewController.m
//  contacts
//
//  Created by Ksendr on 01.11.14.
//  Copyright (c) 2014 Ksendr. All rights reserved.
//

#import "AddViewController.h"
#import "ContactsTableViewController.h"

@interface AddViewController ()
@property (weak, nonatomic) IBOutlet UITextField *tbName;
@property (weak, nonatomic) IBOutlet UITextField *tbPhone;

@end

@implementation AddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)btnSave:(UIButton *)sender {
    UITabBarController* tbc =  (UITabBarController*)self.presentingViewController;
    UINavigationController* navc = (UINavigationController*)tbc.viewControllers[0];
    
    ContactsTableViewController *ctvc = (ContactsTableViewController*)navc.topViewController;
    
    NSString *tmp = self.tbName.text;
    
    [ctvc addContact:tmp :self.tbPhone.text];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)btnCancel:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
