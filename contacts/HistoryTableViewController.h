//
//  HistoryTableViewController.h
//  contacts
//
//  Created by Ksendr on 02.11.14.
//  Copyright (c) 2014 Ksendr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryTableViewController : UITableViewController

- (void) updateHistory;

@end
