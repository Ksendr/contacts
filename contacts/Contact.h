//
//  Contact.h
//  contacts
//
//  Created by Ksendr on 01.11.14.
//  Copyright (c) 2014 Ksendr. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contact : NSObject

@property NSString *name;
@property NSString *phone;

-(id)init:(NSString *)name :(NSString *)phone;
-(void)call;

@end

