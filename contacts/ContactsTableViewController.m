//
//  ContactsTableViewController.m
//  contacts
//
//  Created by Ksendr on 01.11.14.
//  Copyright (c) 2014 Ksendr. All rights reserved.
//

#import "ContactsTableViewController.h"
#import "HistoryTableViewController.h"

@interface ContactsTableViewController ()

@property NSMutableArray *contacts;
@property BOOL editable;

@end

@implementation ContactsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.contacts = [NSMutableArray array];
    self.editable = NO;
    self.history = [[History alloc] init:2];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.contacts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContactCell" forIndexPath:indexPath];
    
    // Configure the cell...
    
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"ContactCell"];
    }
    Contact *contact = (Contact *)[self.contacts objectAtIndex:indexPath.row];
    UILabel *label;
    
    label = (UILabel *)[cell viewWithTag:1];
    label.text = contact.name;
    
    label = (UILabel *)[cell viewWithTag:2];
    label.text = contact.phone;
    
    return cell;
    
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.editable;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [self.contacts removeObjectAtIndex:indexPath.row];
        [tableView beginUpdates];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [tableView endUpdates];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Contact *contact = (Contact *)[self.contacts objectAtIndex:indexPath.row];
    
    [contact call];
    [self addCall:contact];
}

- (void) addCall:(Contact *)contact{
    [self.history addInHistory:contact];
    
    UITabBarController *tbc = (UITabBarController*)self.tabBarController;
    UINavigationController* navc = (UINavigationController*)tbc.viewControllers[1];
    HistoryTableViewController* htvc = (HistoryTableViewController*)navc.viewControllers[0];
    [htvc updateHistory];
}

-(void) addContact:(NSString *)name :(NSString *)phone{
    Contact *contact = [[Contact alloc] init:name :phone];
    [self.contacts addObject:contact];
}
- (IBAction)btnEdit:(UIButton *)sender {
    //self.editable != self.editable;
    if(self.editable){
        self.editable = NO;
        sender.tintColor = [UIColor blueColor];
    }
    else{
        self.editable = YES;
        sender.tintColor = [UIColor redColor];
    }
}

@end
