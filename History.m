//
//  History.m
//  contacts
//
//  Created by Ksendr on 02.11.14.
//  Copyright (c) 2014 Ksendr. All rights reserved.
//

#import "History.h"

@implementation History

-(id)init:(NSUInteger)capacity{
    self = [super init];
    if(self){
        self.dict = [NSMutableDictionary dictionaryWithCapacity:capacity];
    }
    return self;
}

-(void) addInHistory:(Contact *)contact{
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setDateStyle:NSDateFormatterMediumStyle];
    [dateFormater setTimeStyle:NSDateFormatterNoStyle];
    NSString *stringDate = [dateFormater stringFromDate:date];
    
    NSMutableArray *arrContacts;
    
    if([self.dict objectForKey:stringDate]){
        arrContacts = (NSMutableArray *)[self.dict objectForKey:stringDate];
    }
    else{
        arrContacts = [NSMutableArray arrayWithCapacity:1];
    }
    
    [arrContacts addObject:contact];
    [self.dict setObject:arrContacts forKey:stringDate];
}

-(NSArray *)arrayCurrentKey:(NSInteger)index{
    NSArray *keys = [self.dict allKeys];
    NSString *currentKey = [keys objectAtIndex:index];
    return [self.dict objectForKey:currentKey];
}

-(void)clear{
    [self.dict removeAllObjects];
}

@end
